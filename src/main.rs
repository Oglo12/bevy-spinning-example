use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(
                Window {
                    title: "Bevy Spinning World".into(),
                    ..default()
                }
            ),
            ..default()
        }))
        .insert_resource(ClearColor(Color::rgb(0.2, 0.2, 0.2)))
        .add_startup_system(spawn_camera)
        .add_startup_system(spawn_cubes)
        .add_startup_system(spawn_floor)
        .add_startup_system(spawn_light)
        .add_system(spin_cubes)
        .add_system(spin_world)
        .run();
}

fn spin_world(mut query: Query<&mut Transform, Without<StaticObj>>, time: Res<Time>) {
    for mut i in query.iter_mut() {
        i.rotate(Quat::from_rotation_y(0.3 * time.delta_seconds()));
    }
}

fn spin_cubes(mut cquery: Query<&mut Transform, With<SpinCube>>, time: Res<Time>) {
    for mut i in cquery.iter_mut() {
        i.rotate(Quat::from_rotation_x(3.0 * time.delta_seconds()));
        i.rotate(Quat::from_rotation_y(3.0 * time.delta_seconds()));
        i.rotate(Quat::from_rotation_z(3.0 * time.delta_seconds()));
    }
}

fn spawn_light(mut cmds: Commands) {
    cmds.spawn(
        (
            PointLightBundle {
                point_light: PointLight {
                    intensity: 2500.0,
                    shadows_enabled: true,
                    ..default()
                },
                transform: Transform::from_xyz(5.0, 11.0, 5.0),
                ..default()
            },

            StaticObj {},
        )
    );
}

fn spawn_floor(mut cmds: Commands, mut meshes: ResMut<Assets<Mesh>>, mut materials: ResMut<Assets<StandardMaterial>>) {
    cmds.spawn(
        PbrBundle {
            mesh: meshes.add(shape::Plane::from_size(15.0).into()),
            material: materials.add(Color::rgb(0.0, 0.5, 0.5).into()),
            transform: Transform::from_xyz(0.0, -1.0, 0.0),
            ..default()
        }
    );
}

fn spawn_cubes(mut cmds: Commands, mut meshes: ResMut<Assets<Mesh>>, mut materials: ResMut<Assets<StandardMaterial>>) {
    for i in 0..10 {
        let ct = i as f32 * 0.05;

        cmds.spawn(
            (
                PbrBundle {
                    mesh: meshes.add(Mesh::from(shape::Cube {size: 0.7})),
                    material: materials.add(Color::rgb(0.0 + ct, 0.2, 0.5 - ct).into()),
                    transform: Transform::from_xyz(0.0, i as f32, 0.0),
                    ..default()
                },

                SpinCube {},
            )
        );
    }
}

fn spawn_camera(mut cmds: Commands) {
    cmds.spawn(
        (
            Camera3dBundle {
                transform: Transform::from_xyz(0.0, 12.5, 25.5).looking_at(Vec3::new(0.0, 3.0, 0.0), Vec3::Y),
                ..default()
            },

            StaticObj {},
        )
    );
}

#[derive(Component)]
struct SpinCube {}

#[derive(Component)]
struct StaticObj {}
